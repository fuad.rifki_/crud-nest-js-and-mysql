import { Resolver, Query } from "@nestjs/graphql";
import { UsersService } from "./users.service";

@Resolver('User')
export class UserResolver {
    constructor(private usersService: UsersService) {}

    @Query() 
    users() {
        return this.usersService.getUsers();
    }
}
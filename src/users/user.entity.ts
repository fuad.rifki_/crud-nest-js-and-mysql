import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, CreateDateColumn, OneToMany } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { config } from 'dotenv';
import { UserRO } from './user.dto';
import { Product } from '../products/product.entity';

config();

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: 25,
    unique: true,
  })
  username: string;

  @Column('text')
  password: string;

  @CreateDateColumn()
  created: Date;

  @OneToMany(type => Product, product => product.author)
  products: Product[];

  @BeforeInsert()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }

  toResponseObject(showToken = true): UserRO {
    const { id, username, created, token } = this;
    const responseObject: any = { id, username, created };
    if (showToken) {
      responseObject.token = token;
    }
    if (this.products) {
      responseObject.products = this.products;
    }
    return responseObject;
  }

  async comparePassword(attempt: string) {
    return await bcrypt.compare(attempt, this.password);
  }

  private get token(): string {
    const { id, username } = this;
    return jwt.sign(
      {
        id,
        username,
      },
      process.env.SECRET,
      { expiresIn: '7d' },
    );
  }
}

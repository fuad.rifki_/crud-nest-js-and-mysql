import { Controller, Get, Post, Body, UsePipes, ValidationPipe, UseGuards, Param } from '@nestjs/common';
import { UsersService } from './users.service';
import { UserDTO } from './user.dto';
import { AuthGuard } from '../shared/auth.guard';
import { User } from './user.decorator';

@Controller()
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get('api/users')
  @UseGuards(new AuthGuard())
  showAllUsers(@User() user) {
    console.log(user);
    return this.usersService.getUsers();
  }

  @Get('api/users/:username')
  async getUser(@Param() params) {
    return await this.usersService.getUser(params.username);
  }

  @Post('login')
  @UsePipes(new ValidationPipe())
  login(@Body() data: UserDTO) {
    return this.usersService.login(data);
  }

  @Post('register')
  @UsePipes(new ValidationPipe())
  register(@Body() data: UserDTO) {
    return this.usersService.register(data);
  }
}

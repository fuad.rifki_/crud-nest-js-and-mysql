import { Resolver, Query } from "@nestjs/graphql";
import { ProductsService } from "./products.service";

@Resolver('Product')
export class ProductResolver {
    constructor(private productService: ProductsService) {}

    @Query()
    products() {
        return this.productService.getProducts();
    }
}
import { Controller, Get, Param, Post, Body, Put, Delete, UsePipes, Logger, UseGuards } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ValidationPipe } from '../shared/validation.pipe';
import { ProductDTO } from './product.dto';
import { AuthGuard } from '../shared/auth.guard';
import { User } from '../users/user.decorator';

@Controller('api/products')
export class ProductsController {
  private logger = new Logger('ProductsController');
  constructor(private productsService: ProductsService) {}

  private logData(options: any) {
    options.user && this.logger.log('USER: ' + JSON.stringify(options.user));
    options.data && this.logger.log('DATA: ' + JSON.stringify(options.data));
    options.id && this.logger.log('PRODUCT: ' + JSON.stringify(options.id));
  }

  @Get()
  async getAll() {
    return await this.productsService.getProducts();
  }

  @Get(':id')
  async get(@Param() params) {
    return await this.productsService.getProduct(params.id);
  }

  @Post('create')
  @UseGuards(new AuthGuard())
  @UsePipes(new ValidationPipe())
  create(@User('id') user, @Body() data: ProductDTO) {
    this.logData({ user, data });
    return this.productsService.createProduct(user, data);
  }

  @Put(':id/update')
  @UseGuards(new AuthGuard())
  @UsePipes(new ValidationPipe())
  async update(@Param('id') id, @User('id') user: number, @Body() data: Partial<ProductDTO>): Promise<any> {
    this.logData({ id, user, data });
    return this.productsService.updateProduct(id, user, data);
  }

  @Delete(':id/delete')
  @UseGuards(new AuthGuard())
  async deleteProduct(@Param('id') id: number, @User('id') user: number) {
    this.logData({ id, user });
    return this.productsService.deleteProduct(id, user);
  }
}

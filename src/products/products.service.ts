import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product, ProductRO } from './product.entity';
import { ProductDTO } from './product.dto';
import { Repository } from 'typeorm';
import { User } from '../users/user.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  private toResponseObject(product: Product): ProductRO {
    return { ...product, author: product.author.toResponseObject(false) };
  }

  private ensureOwnership(product: Product, userId: number) {
    if (product.author.id !== userId) {
      throw new HttpException('Incorrect user', HttpStatus.UNAUTHORIZED);
    }
  }

  async getProducts(): Promise<ProductRO[]> {
    const products = await this.productRepository.find({ relations: ['author'] });
    return products.map(product => this.toResponseObject(product));
  }

  async getProduct(id: number): Promise<ProductRO> {
    const product = await this.productRepository.findOne({ where: { id }, relations: ['author'] });
    if (!product) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }
    return this.toResponseObject(product);
  }

  async createProduct(userId: string, data: ProductDTO): Promise<ProductRO> {
    const { name } = data;
    const user = await this.userRepository.findOne({ where: { id: userId } });
    let product = await this.productRepository.findOne({ where: { name } });
    if (product) {
      throw new HttpException('Product already exist', HttpStatus.BAD_REQUEST);
    }
    product = await this.productRepository.create({ ...data, author: user });
    await this.productRepository.save(product);
    return this.toResponseObject(product);
  }

  async updateProduct(id: number, userId: number, data: Partial<ProductDTO>): Promise<ProductRO> {
    let product = await this.productRepository.findOne({ where: { id }, relations: ['author'] });
    if (!product) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }
    this.ensureOwnership(product, userId);
    await this.productRepository.update({ id }, data);
    product = await this.productRepository.findOne({ where: { id }, relations: ['author'] });
    return this.toResponseObject(product);
  }

  async deleteProduct(id: number, userId: number) {
    const product = await this.productRepository.findOne({ where: { id }, relations: ['author'] });
    if (!product) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }
    this.ensureOwnership(product, userId);
    await this.productRepository.delete(id);
    return { message: 'Data successfully deleted!' };
  }
}
